import React from 'react';
import dadosMenu from '../../../dados/Menu';
import ItemMenu from '../../atoms/ItemMenu';
import Button from '@material-ui/core/Button';
//Styled components
import { Menustyled } from './style';
import { useStateValue } from '../../../state';
import { setBarraLateral } from '../../../Reducer';
import ButtonContato from './ButtonContato';
const Menu = ({ docked }) => {
	const [ { barraLateral }, dispatch ] = useStateValue();
	const handleBarra = () => {
		dispatch(setBarraLateral({ open: !barraLateral.open }));
	};
	return (
		<Menustyled className="d-flex justify-content-end align-items-center" docked={docked}>
			{dadosMenu.map((opcao) => (
				<div className="mr-2">
					<ItemMenu to={opcao.link}>{opcao.item}</ItemMenu>
				</div>
			))}

			<ButtonContato onClick={handleBarra}>Contato</ButtonContato>
		</Menustyled>
	);
};

export default Menu;
