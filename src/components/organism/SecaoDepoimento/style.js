import styled, { css } from 'styled-components';
import Row from '../../atoms/Row';
export const RowStyled = styled(Row)`
    padding-top: 50px;
    padding-bottom: 50px;
    height: 100%;
    background-position: bottom;
`;
export const ContentNossoNumeros = styled.div`
    padding-top: 20%;
    padding-bottom: 10px;
`;

export const ContentIconeLateral = styled.div`
  
`;
