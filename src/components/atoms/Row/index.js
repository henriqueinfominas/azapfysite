import React from 'react';
import { RowStyled } from './style';
const Row = ({ backgroundImage, children, ...props }) => {
	return (
		<RowStyled backgroundImage={backgroundImage} {...props}>
			{children}{' '}
		</RowStyled>
	);
};

export default Row;
