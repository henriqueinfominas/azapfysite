import React from 'react';

import { NavItemContent } from './style';

const NavItem = ({ children, ...props }) => <NavItemContent {...props}>{children}</NavItemContent>;

export default NavItem;
