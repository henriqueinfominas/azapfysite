import React from "react";

import IconeValor from "../../atoms/IconeValor";

import { faMapMarkerAlt,faPaperPlane, faPhone } from "@fortawesome/free-solid-svg-icons";

const CardContato = () => {
  return (
    <div className="d-flex flex-column">
      <IconeValor
        icon={faMapMarkerAlt}
        cor="transparent"
        textCor="#fff"
        iconCor="#FF8000"
        transparencia={false}
        texto="Avenida Afonso Pena, 867 5º Andar - Centro/BH"
      />
       <IconeValor
        icon={faPhone}
        cor="transparent"
        textCor="#fff"
        iconCor="#FF8000"
        transparencia={false}
        texto="(31) 3213-8724"
      />
       <IconeValor
        icon={faPhone}
        cor="transparent"
        textCor="#fff"
        iconCor="#FF8000"
        transparencia={false}
        texto="(31) 3213-8783"
      />
       <IconeValor
        icon={faPaperPlane}
        cor="transparent"
        textCor="#fff"
        iconCor="#FF8000"
        transparencia={false}
        texto="contato@azapfy.com.br"
      />
    </div>
  );
};

export default CardContato;
