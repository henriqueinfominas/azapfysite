import React from 'react';
import TypographyStyled from '../../atoms/Typography';
import { RowStyled } from './style';
import CardItens from './CardItens';
import ScrollAnimation from 'react-animate-on-scroll';
import ButtonConsultor from '../../moleculus/ButtonConsultor';
const Sobre = () => {
	return (
		<section>
			<RowStyled backgroundColor="#F1F1F1">
				<div className="container">
					<div className="d-flex flex-column align-items-center">
						<ScrollAnimation animateOnce animateIn="fadeInDown">
							<TypographyStyled variant="h4" className="mt-5">
								Como funciona
							</TypographyStyled>
						</ScrollAnimation>
						<ScrollAnimation animateOnce animateIn="fadeInDown">
							<TypographyStyled variant="p" component="p" align="center">
								Informação em tempo real, que garante a satisfação do seu
								<br />
								cliente e maior segurança para sua operação.
							</TypographyStyled>
						</ScrollAnimation>
						<CardItens />

						<div className="d-flex justify-content-center w-100 mt-5">
							<ButtonConsultor />
						</div>
					</div>
				</div>
			</RowStyled>
		</section>
	);
};

export default Sobre;
