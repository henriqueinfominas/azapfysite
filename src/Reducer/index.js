export const initialState = {
	barraLateral: {
		open: false
	}
};
export const reducer = (state, action) => {
	switch (action.type) {
		case 'setState':
			return {
				...state,
				...action.payload
			};
		case 'setBarraLateral':
			return {
				...state,
				barraLateral: {
					...state.barraLateral,
					...action.payload
				}
			};

		default:
			return state;
	}
};

//Açoes
export const setState = (payload) => ({
	type: 'setState',
	payload
});
export const setBarraLateral = (payload) => ({
	type: 'setBarraLateral',
	payload
});
