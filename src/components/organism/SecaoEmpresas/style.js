import styled, { css } from "styled-components";

import Row from "../../atoms/Row";
export const RowStyled = styled(Row)`
  padding-top: 50px;
  padding-bottom: 50px;
`;

export const ImagemEmpresa = styled.img`
  height: 60px;
  margin: 20px;
`;
