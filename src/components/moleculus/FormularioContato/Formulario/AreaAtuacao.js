import React from "react";

import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import { InputContent, InputLabelStyled, SelectStyled } from "./style";
const lista = [
  { nome: "Transportadora de Medicamentos" },
  { nome: "Transportadora Geral" },
  { nome: "Distribuidora de Medicamentos" },
  { nome: "Distribuidora Geral" },
  { nome: "Motoristas" },
  { nome: "Outros" }
];
const AreaAtuacao = ({ label, value, onChange, onBlur, className }) => {
  return (
    <InputContent className={className}>
      <InputLabelStyled htmlFor="areaAtuacao">{label}</InputLabelStyled>
      <SelectStyled
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        inputProps={{
          name: "areaAtuacao",
          id: "areaAtuacao"
        }}
      >
        <MenuItem value="">
          <em>Nenhum</em>
        </MenuItem>
        {lista.map((item, index) => (
          <MenuItem value={item.value ? item.value : item.nome}>
            {item.nome}
          </MenuItem>
        ))}
      </SelectStyled>
    </InputContent>
  );
};

export default AreaAtuacao;
