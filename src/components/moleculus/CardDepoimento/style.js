import styled, { css } from "styled-components";

export const CardStyled = styled.div`
  && {
    max-width: 500px;
    height: 100%;
    background-color: #fff;
    border-radius: 10px;
    box-shadow: 0px 2px 6px 0px rgba(0, 0, 0, 0.05);
    padding: 40px;
  }
`;
export const ContentIcon = styled.div`
  bottom: 60px;
  position: relative;
`;
