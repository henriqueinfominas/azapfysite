import React from 'react';
import IconeAvatar from '../../../atoms/IconeAvatar';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import TypographyStyled from '../../../atoms/Typography';
import {ContentStyled} from './style';
const MensagemConfirmacao = ({ email }) => (
	<ContentStyled className="d-flex flex-column justify-content-center align-items-center">
		<IconeAvatar icon={faCheckCircle} cor="transparent"  transparencia={false} avatarSize="60px" iconSize="2x"/>
		<TypographyStyled variant="h4" align="center">Mensagaem enviada com sucesso!</TypographyStyled>
		<TypographyStyled variant="p" align="center">Em breve entraremos em contato, você receberá mais informações no e-mail {email}</TypographyStyled>
	</ContentStyled>
);
export default MensagemConfirmacao;
