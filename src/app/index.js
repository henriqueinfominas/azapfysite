import React from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import Home from "./routes/Home";
import Contato from "./routes/Contato";
import defaultTheme from "../containers/themes/defaultTheme";

const Aplicativo = ({ match }) => {
  const applyTheme = createMuiTheme(defaultTheme);
  return (
    <MuiThemeProvider theme={applyTheme}>
      <BrowserRouter >
        <Switch>
          <Route exact path={`/`} component={Home} />
          <Route path={`/contato`} component={Contato} />
        </Switch>
      </BrowserRouter>
    </MuiThemeProvider>
  );
};

export default Aplicativo;
