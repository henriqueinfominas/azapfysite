import React from 'react';
import { ContentStyled } from './style';
const ContentPaginas = ({ backgroundImage, children, ...props }) => {
	return <ContentStyled {...props}>{children}</ContentStyled>;
};

export default ContentPaginas;
