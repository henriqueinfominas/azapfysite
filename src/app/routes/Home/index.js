import React from 'react';

import Header from '../../../components/organism/Header';
import BarraLateral from '../../../components/organism/BarraLateral';
import Slider from '../../../components/organism/Slider';
import SecaoMapa from '../../../components/organism/SecaoMapa';
import SecaoSobre from '../../../components/organism/SecaoSobre';
import SecaoCrescer from '../../../components/organism/SecaoCrescer';
import SliderEtapas from '../../../components/organism/SliderEtapas';
import SecaoDepoimento from '../../../components/organism/SecaoDepoimento';
import SecaoEmpresas from '../../../components/organism/SecaoEmpresas';
import SecaoComoFunciona from '../../../components/organism/SecaoComoFunciona';
import CallToAction from '../../../components/organism/CallToAction';
import Footer from '../../../components/organism/Footer';
import ConteudoContato from '../../../components/organism/Contato';
import { Parallax, ParallaxLayer } from 'react-spring/renderprops-addons';
const Home = () => (
	<div>
		<Header />
		<BarraLateral>
			<ConteudoContato />
		</BarraLateral>
		<Slider />
		<SecaoSobre />
		<SliderEtapas />
		<SecaoMapa />
		<SecaoDepoimento />
		<SecaoEmpresas />
		<SecaoCrescer />
		<SecaoComoFunciona />
		<CallToAction />
		<Footer />
	</div>
);

export default Home;
