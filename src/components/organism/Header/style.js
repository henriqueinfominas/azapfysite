import styled, { css } from 'styled-components';
import AppBar from '@material-ui/core/AppBar';
import { NavItemContent } from '../../atoms/ItemMenu/style';
export const AppBartyled = styled(AppBar)`
	&&{
        box-shadow: ${(props) => (props.docked ? '0 3px 16px 0 rgba(0,0,0,.1)' : 'none')};
        background-color: ${(props) => (props.docked ? '#fff' : 'transparent')};
    }
`;
