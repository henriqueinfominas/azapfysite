import React from "react";
import { ImageOverlayStyled } from "./style";
const ImageOverlay = ({ children, ...props }) => {
  return <ImageOverlayStyled {...props}>{children}</ImageOverlayStyled>;
};

export default ImageOverlay;
