import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

import Toolbar from "@material-ui/core/Toolbar";

import Menu from "../../moleculus/Menu";
import MenuMobile from "../../moleculus/MenuMobile";
import MenuMobileModal from "../../organism/MenuMobile";
import MenuMobileConteudo from "../../moleculus/MenuMobile/Conteudo";
import { LogoWebBranca, LogoWeb } from "../../atoms/Logo";
import { NavLink } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faTimes } from "@fortawesome/free-solid-svg-icons";
import IconButton from "@material-ui/core/IconButton";
//Styled components
import { AppBartyled } from "./style";
const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

const AppBar = props => {
  const { classes } = props;
  const [docked, setDocked] = useState(true);
  const [mobile, setMobile] = useState(window.innerWidth > 700 ? false : true);
  const [mostrarMenuMobile, setMostrarMenuMobile] = useState(false);
  useEffect(() => {
    changeHeaderScoll();
    changeMenuResize();
  });
  const handleClickMenuMobile = () => {
    setMostrarMenuMobile(!mostrarMenuMobile);
    //setDocked(!mostrarMenuMobile);
  };
  const changeMenuResize = () => {
    window.addEventListener("resize", event => {
      window.innerWidth > 700 ? setMobile(false) : setMobile(true);
    });
  };
  const changeHeaderScoll = () => {
    if (!props.docked) {
      window.addEventListener("scroll", event => {
        if (window.scrollY > 100) {
          if (!docked) {
            //setDocked(true);
          }
        } else {
          if (docked) {
            //setDocked(false);
          }
        }
      });
    }
  };
  return (
    <AppBartyled position="fixed" className={`${classes.root}`} docked={docked}>
      <Toolbar className="pr-0">
        <div className={`d-flex flex-column align-items-center w-100`}>
          <div
            className={`d-flex justify-content-between align-items-center flex-wrap w-100`}
          >
            <NavLink to="/">
              {docked ? <LogoWeb height={40} /> : <LogoWebBranca height={40} />}
            </NavLink>

            {mobile ? (
              <MenuMobile
                docked={docked}
               
                onClick={handleClickMenuMobile}
              />
            ) : (
              <Menu docked={docked} />
            )}
          </div>
          {mobile && mostrarMenuMobile && (
            <MenuMobileConteudo docked={docked} />
          )}
          <MenuMobileModal open={mostrarMenuMobile} onClose={handleClickMenuMobile}/>
        </div>
      </Toolbar>
    </AppBartyled>
  );
};

AppBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AppBar);
