import styled from "styled-components";
import Row from "../../atoms/Row";
export const CarouselCaption = styled.div`
  position: relative;
  top: 0;
  bottom: 0;
  z-index: 10;
  color: #fff;
  text-align: center;
  width: 100%;
  height: 100%;
`;
export const SliderContent = styled.div`
  width: 100%;
`;
export const SliderImagem = styled.img`
  height: 100vh;
`;
export const RowSlider = styled(Row)`
  min-height: 100vh;
`;
