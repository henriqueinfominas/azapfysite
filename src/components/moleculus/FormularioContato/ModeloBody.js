import React from 'react';

const ModeloBody = ({ nome, cargo, empresa, telefone, areaAtuacao }) => (
	<div>
		<h4>{nome}</h4>
		<p>
			<b>Cargo: </b> {cargo}
		</p>
		<p>
			<b>Empresa: </b> {empresa}
		</p>
		<p>
			<b>Área de atuação: </b> {areaAtuacao}
		</p>
		<p>
			<b>Telefone: </b> {telefone}
		</p>
	</div>
);
export default ModeloBody;
