import React from 'react';
import MapaConquistas from '../../../assets/images/mapa-conquistas.webp';
import Rastros from '../../../assets/images/background-rastros.webp';

import TypographyStyled from '../../atoms/Typography';
import Numeros from './Numeros';
import RowParallax from '../../atoms/RowParallax';
import ScrollAnimation from 'react-animate-on-scroll';
const SecaoMapa = () => {
	return (
		<section>
			<RowParallax
				layers={[
					{
						image: Rastros,
						amount: 0.5
					}
				]}
				className="d-flex align-items-center"
			>
				<div className="container ">
					<div className="row">
						<div className="col-12 col-lg-6 ">
							<ScrollAnimation animateOnce animateIn="fadeInDown">
								<TypographyStyled variant="h5" className="mt-5">
								Oferecemos uma solução capaz de automatizar a comprovação e mudar a forma de gerir as suas coletas e entregas.
								</TypographyStyled>
							</ScrollAnimation>
							<ScrollAnimation animateOnce animateIn="fadeInDown">
								<TypographyStyled variant="p" component="p">
								Oferecemos uma solução capaz de automatizar a comprovação e mudar a forma de gerir as suas coletas e entregas.

								</TypographyStyled>
							</ScrollAnimation>
							<Numeros />
						</div>
						<div className="col-12 col-lg-6 ">
							<ScrollAnimation animateOnce animateIn="fadeIn" delay="1">
								<img className="img-fluid" src={MapaConquistas} alt="Map conquistas" />
							</ScrollAnimation>
						</div>
					</div>
				</div>
			</RowParallax>
		</section>
	);
};

export default SecaoMapa;
