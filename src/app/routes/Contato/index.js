import React from 'react';
import ContentPaginas from '../../../components/atoms/ContentPaginas';
import Header from '../../../components/organism/Header';
import PageTitle from '../../../components/moleculus/PageTitle';
import ConteudoContato from '../../../components/organism/Contato';
import Footer from '../../../components/organism/Footer';

const Contato = () => (
	<div>
		<ContentPaginas>
    <Header docked />
			<PageTitle titulo="Contato" />
			<ConteudoContato />
		</ContentPaginas>
		<Footer />
	</div>
);

export default Contato;
