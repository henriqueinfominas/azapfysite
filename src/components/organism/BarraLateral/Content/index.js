import React from 'react';
import { ContentStyled } from './style';
import { useStateValue } from '../../../../state';
import { setBarraLateral } from '../../../../Reducer';
import IconeAvatar from '../../../atoms/IconeAvatar';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import ClearIcon from '@material-ui/icons/Clear';
import { primary } from '../../../../containers/themes/defaultTheme';
import Icon from '@material-ui/core/Icon';
import TypographyStyled from '../../../atoms/Typography';
const ContentBarraLateral = ({ esconder, children }) => {
	const [ { barraLateral }, dispatch ] = useStateValue();
	const handleFechar = () => {
		dispatch(setBarraLateral({ open: false }));
	};
	return (
		<ContentStyled className={`animated ${barraLateral.open ? 'slideInRight' : 'slideOutRight'} col-lg-4 col-sm-6 col-12`}>
			<div className="container">
				<div className="d-flex justify-content-end">
					<Tooltip title="Fechar">
						<IconButton onClick={handleFechar} size="small" color={primary}>
							<ClearIcon color="primary" />
						</IconButton>
					</Tooltip>
				</div>
				<div className="mt-2">
					{children}
				</div>
			</div>
		</ContentStyled>
	);
};
export default ContentBarraLateral;
