import React from 'react';
import { ContentStyled, ContentTypography } from './style';
import IconeAvatar from '../IconeAvatar';
const IconeValor = ({
	coluna,
	icon,
	cor,
	valor,
	iconSize,
	avatarSize,
	tipo,
	texto,
	onClick,
	textCor,
	valorCor,
	iconCor,
	transparencia,
	img,
	variantTexto,
	variantValor,
	...props
}) => {
	const handleClick = () => {
		onClick && onClick({ tipo, valor, texto });
	};
	return (
		<ContentStyled onClick={handleClick} action={onClick} {...props}>
			<div className={`d-flex ${coluna ? 'flex-column align-items-center' : ''}`}>
				<IconeAvatar
					icon={icon}
					cor={cor ? cor : '#ccc'}
					hover={onClick}
					transparencia={transparencia}
					iconCor={iconCor}
					img={img}
					iconSize={iconSize}
					avatarSize={avatarSize}
				/>
				<div className="d-flex flex-column justify-content-center ml-2 ">
					{valor && (
						<ContentTypography
							variant={variantValor ? variantValor : 'body2'}
							color={valorCor}
							align={coluna ? 'center' : 'left'}
						>
							{valor}
						</ContentTypography>
					)}
					{texto && (
						<ContentTypography
							variant={variantTexto ? variantTexto : 'caption'}
							color={textCor}
							align={coluna ? 'center' : 'left'}
						>
							{texto}
						</ContentTypography>
					)}
				</div>
			</div>
		</ContentStyled>
	);
};
export default IconeValor;
