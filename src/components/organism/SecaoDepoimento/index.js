import React from 'react';
import TypographyStyled from '../../atoms/Typography';
import CardDepoimento from '../../moleculus/CardDepoimento';
import AvatarMaria from '../../../assets/images/avatar/avatar-maria.webp';
import AvatarMoycana from '../../../assets/images/avatar/avatar-moicana.webp';
import { RowStyled } from './style';
import { primary, secondary } from '../../../containers/themes/defaultTheme';
import { SvgLoader, SvgProxy } from 'react-svgmt';
import DesignerCurva from '../../../assets/images/designer-curva.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuoteRight } from '@fortawesome/free-solid-svg-icons';
import ImageOverlay from '../../atoms/ImageOverlay';
import ScrollAnimation from 'react-animate-on-scroll';
const SecaoDepoimento = () => {
	return (
		<section>
			<RowStyled
				backgroundColor={primary}
				layers={[
					{
						children: <ImageOverlay backgroundColor={primary} />,
						amount: 0.2
					}
				]}
			>
				<div className="container">
					<div className="row">
						<ScrollAnimation animateOnce animateIn="fadeInDown" className="w-100">
							<TypographyStyled align="center" variant="h4" className="mt-5 w-100" color="#ffffff">
								Depoimentos de nossos clientes
							</TypographyStyled>
						</ScrollAnimation>
						<div className="mt-5 d-flex justify-content-center w-100 flex-wrap flex-xl-nowrap flex-lg-nowrap">
							<ScrollAnimation animateOnce animateIn="fadeInRight" className="m-3">
								<CardDepoimento
									className="m-2"
									nome="Maria Assunção"
									empresa="Transmed Assunção"
									cargo="Gerente Administrativa Financeira"
									avatarSize="60px"
									img={<img src={AvatarMaria} width="60px" />}
									texto="“Quando começamos a utilizar o Azapfy nós tinhamos uma média de 250 a 300 entregas por dia. Nós tínhamos uma única pessoa que fazia o tratamento dessa informação que era do canhoto. Hoje nós utilizamos o Azapfy a aproximadamente uns 2 anos e hoje nós temos um volume que é praticamente o dobro e eu continuo tendo uma única pessoa fazendo esse trabalho.”"
								/>
							</ScrollAnimation>
							<ScrollAnimation animateOnce animateIn="fadeInRight" className="m-2">
								<CardDepoimento
									className="m-2"
									nome="Fabiana Viana"
									empresa="Transportadora Moycanacana"
									cargo="Diretora Administrativa"
									avatarSize="60px"
									img={<img src={AvatarMoycana} width="60px" />}
									texto="“Quando o Azapfy veio pra poder resolver o problema, outras empresas já tinham passado e já tinham oferecido soluções, só que a grande diferença foi a forma de integrar, de relacionar com nosso ERP, e entrar em contato com o pessoal, fazer a ponte mesmo entre a transportadora e o ERP. Facilitar, mostrar pra gente a solução de uma forma descomplicada, de uma forma mais clara e passando segurança pra gente mesmo. Acho que a leitura é fácil, a comunicação é fácil, principalmente pro usuário nosso de transportadora que é um motorista que é um ajudante isso facilita bastante.”"
								/>
							</ScrollAnimation>
						</div>
						<div className="mt-5 d-flex justify-content-end w-100">
							<ScrollAnimation animateOnce animateIn="fadeInUp">
								<FontAwesomeIcon icon={faQuoteRight} size="6x" color={secondary} />
							</ScrollAnimation>
						</div>
						<ScrollAnimation animateOnce animateIn="fadeInDown">
							<TypographyStyled
								align="center"
								variant="h4"
								className="mt-5 w-100 animated fadeIn"
								color="#ffffff"
							>
								Conheça alguns dos clientes que confiam em nosso trabalho
							</TypographyStyled>
						</ScrollAnimation>
					</div>
				</div>
			</RowStyled>
			<ScrollAnimation animateOnce animateIn="slideInDown">
				<SvgLoader path={DesignerCurva} width="100%" />
			</ScrollAnimation>
		</section>
	);
};

export default SecaoDepoimento;
