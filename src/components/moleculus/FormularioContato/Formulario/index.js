import React from 'react';
import { Formik } from 'formik';
import Button from '../../../atoms/Button';
import FormHelperText from '@material-ui/core/FormHelperText';
import AreaAtuacao from './AreaAtuacao';
import { TextFieldStyled } from './style';
const initialValues = {
	nome: '',
	email: '',
	empresa: '',
	cargo: '',
	telefone: '',
	areaAtuacao: ''
};
const Formulario = ({ onEnviarEmail }) => {
	return (
		<Formik
			initialValues={initialValues}
			validate={(values) => {
				let errors = {};

				if (!values.email) {
					errors.email = 'Obrigatório';
				} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
					errors.email = 'E-mail inválido';
				}
				return errors;
			}}
			onSubmit={(values, actions) => {
				onEnviarEmail({ values, actions });
			}}
		>
			{({
				values,
				errors,
				touched,
				handleChange,
				handleBlur,
				handleSubmit,
				isSubmitting
				/* and other goodies */
			}) => (
				<form onSubmit={handleSubmit} className="w-100">
					{errors.enviar && (
						<FormHelperText id="component-error-text" error={true}>
							{errors.enviar}
						</FormHelperText>
					)}
					<div className="d-flex flex-wrap  w-100">
						<TextFieldStyled
							type="text"
							name="nome"
							label="Nome"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.nome}
							className="m-2"
							required
							error={errors.nome ? true : false}
						/>
						{errors.nome &&
						touched.nome && (
							<FormHelperText id="component-error-text" error={true}>
								{errors.nome}
							</FormHelperText>
						)}

						<TextFieldStyled
							type="email"
							name="email"
							label="E-mail"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.email}
							className="m-2"
							required
							error={errors.email ? true : false}
						/>
						{errors.email &&
						touched.email && (
							<FormHelperText id="component-error-text" error={true}>
								{errors.email}
							</FormHelperText>
						)}
					</div>

					<div className="d-flex flex-wrap ">
						<TextFieldStyled
							type="text"
							name="empresa"
							label="Empresa"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.empresa}
							className="m-2"
							required
							error={errors.empresa ? true : false}
						/>
						{errors.empresa &&
						touched.empresa && (
							<FormHelperText id="component-error-text" error={true}>
								{errors.empresa}
							</FormHelperText>
						)}

						<TextFieldStyled
							type="text"
							name="cargo"
							label="Cargo"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.cargo}
							className="m-2"
							error={errors.cargo ? true : false}
							required
						/>
						{errors.cargo &&
						touched.cargo && (
							<FormHelperText id="component-error-text" error={true}>
								{errors.cargo}
							</FormHelperText>
						)}
					</div>

					<div className="d-flex flex-wrap ">
						<TextFieldStyled
							type="text"
							name="telefone"
							label="Telefone"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.telefone}
							className="m-2 mt-4"
							required
							error={errors.telefone ? true : false}
						/>
						{errors.telefone &&
						touched.telefone && (
							<FormHelperText id="component-error-text" error={true}>
								{errors.telefone}
							</FormHelperText>
						)}

						<AreaAtuacao
							name="areaAtuacao"
							label="Mercado de Atuação"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.areaAtuacao}
							className="m-2"
							error={errors.areaAtuacao ? true : false}
						/>
						{errors.areaAtuacao &&
						touched.areaAtuacao && (
							<FormHelperText id="component-error-text" error={true}>
								{errors.areaAtuacao}
							</FormHelperText>
						)}
					</div>

					<div className="d-flex justify-content-center mt-3">
						<Button
							variant="contained"
							color="primary"
							type="submit"
							disabled={
								isSubmitting ||
								Object.keys(values).filter((item) => values[item] == '').length > 0 ||
								Object.keys(errors).length > 0
							}
						>
							ENVIAR
						</Button>
					</div>
				</form>
			)}
		</Formik>
	);
};

export default Formulario;
