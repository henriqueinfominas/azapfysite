import styled, { css } from 'styled-components';

import Row from '../../atoms/Row';
export const RowStyled = styled(Row)`
    padding-top: 50px;
    padding-bottom: 50px;
`;

export const CardItensContent = styled.div`margin-top: 50px;`;
