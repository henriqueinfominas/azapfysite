import styled, { css } from "styled-components";

export const ImageOverlayStyled = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  text-align: center;
  background-repeat: no-repeat, repeat;
	background-size: cover;
	background-position: center;
  ${(props) =>
		props.backgroundColor && !props.overlay && css`background-color: ${(props) => props.backgroundColor};`};
	${(props) =>
		props.overlay &&
		css`
			background: linear-gradient(rgba(0, 0, 0, 0.60), rgba(0, 0, 0, 0.60)),
				url(${(props) => props.backgroundImage});
		`};

`;
