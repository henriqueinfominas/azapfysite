import React from 'react';
import { TypographyStyled } from './style';
const Typography = ({ color, children, ...props }) => {
	return (
		<TypographyStyled color={color} {...props}>
			{children}
		</TypographyStyled>
	);
};

export default Typography;
