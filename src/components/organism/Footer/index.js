import React from 'react';
import ImagemFooter from '../../../assets/images/imagem-footer.webp';
import { LogoBranca } from '../../atoms/Logo';

//Styled components
import ImageOverlay from '../../atoms/ImageOverlay';
import CardSobre from './CardSobre';
import CardContato from './CardContato';
import { RowFooter, FooterStyled, ContentFooter } from './style';
import { Parallax, ParallaxLayer } from 'react-spring/renderprops-addons';

const Footer = () => {
	return (
		<FooterStyled>
			<Parallax pages={1} style={{ overflow: 'none', height: '500px' }} scrolling={false}>
				<ParallaxLayer
					offset={0}
					speed={-0.3}
					factor={3}
					style={{
						backgroundImage: `url(${ImagemFooter})`,
						backgroundSize: 'cover',
						height: '100%',
						backgroundPosition: 'center'
					}}
				/>

				<ParallaxLayer offset={0} speed={1} style={{ backgroundColor: 'rgba(0,0,0,0.5)' }} />
				<ParallaxLayer
					offset={0}
					speed={0.1}
					style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
				>
					<ContentFooter className="container d-flex align-items-center">
						<div className="row">
							<div className="col-12 col-lg-4 mb-3 ">
								<CardSobre />
							</div>
							<div className="col-12 col-lg-4 mb-3 ">
								<div className="d-flex justify-content-center">
									<LogoBranca width="200px" height="100%" className="" />
								</div>
							</div>
							<div className="col-12 col-lg-4 mb-3 ">
								<CardContato />
							</div>
						</div>
					</ContentFooter>
				</ParallaxLayer>
			</Parallax>
		</FooterStyled>
	);
};

export default Footer;
