import React, { Component } from 'react';
import './styles/style.scss';
import Aplicacao from './app/index.js';
import { ParallaxProvider } from 'react-scroll-parallax';
import { StateProvider } from './state';
import { initialState, reducer } from './Reducer';
class App extends Component {
	render() {
		return (
			<div className="App">
				<ParallaxProvider>
					<StateProvider initialState={initialState} reducer={reducer}>
						<Aplicacao />
					</StateProvider>
				</ParallaxProvider>
			</div>
		);
	}
}

export default App;
