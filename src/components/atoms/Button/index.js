import React from 'react';
import { ButtonStyled } from './style';
const Button = ({ children, ...props }) => {
	return <ButtonStyled {...props}>{children}</ButtonStyled>;
};

export default Button;
