import React from 'react';
import dadosMenu from '../../../dados/Menu';
import ItemMenu from '../../atoms/ItemMenu';
//Styled components
import { Menustyled } from './style';

const Menu = ({ docked, ...props }) => (
	<Menustyled
		className="d-flex flex-column align-items-center justify-content-center w-100"
		docked={docked}
		{...props}
	>
		{dadosMenu.map((opcao, index) => (
			<div className={`m-2 animated fadeInDown `}>
				<ItemMenu to={opcao.link}>{opcao.item}</ItemMenu>
			</div>
		))}
	</Menustyled>
);

export default Menu;
