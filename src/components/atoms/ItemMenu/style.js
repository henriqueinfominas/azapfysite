import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

export const NavItemContent = styled(NavLink)`
color: ${(props) => (props.color ? props.color : '#fff')};
font-weight:bold;
text-transform: uppercase;
    :hover{
        color: ${(props) => (props.color ? props.color : '#fff')};
        text-decoration: none;
        
    }
    `;
