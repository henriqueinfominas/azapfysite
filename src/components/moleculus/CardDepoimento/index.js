import React from "react";
import { CardStyled, ContentIcon } from "./style";
import IconeAvatar from "../../atoms/IconeAvatar";
import TypographyStyled from "../../atoms/Typography";
import { secondary } from "../../../containers/themes/defaultTheme";
const CardDepoimento = ({
  img,
  icon,
  texto,
  nome,
  empresa,
  cargo,
  avatarSize,
  ...props
}) => {
  return (
    <CardStyled {...props}>
      <div className="d-flex justify-content-center align-items-center">
        <ContentIcon>
          <IconeAvatar
            img={img}
            icon={icon}
            transparencia={false}
            cor="#fff"
            radius={true}
            avatarSize={avatarSize}
          />
        </ContentIcon>
      </div>
      <TypographyStyled variant="p" component="p">
        {texto}
      </TypographyStyled>
      <TypographyStyled variant="caption" className="mt-5" color={secondary}>
        {nome} - {empresa}
        <br /> {cargo}
      </TypographyStyled>
    </CardStyled>
  );
};

export default CardDepoimento;
