import styled, { css } from 'styled-components';
import Button from '@material-ui/core/Button';
import { secondary } from '../../../../containers/themes/defaultTheme';
import Color from 'color';
export const ButtonStyled = styled(Button)`
&&{
    height: 64px;
    background-color: ${secondary};
    color: #fff;
    border-radius: 0px;
    
    :hover{
        background-color: ${Color(secondary).darken(0.3).string()};
    }
}
`;
