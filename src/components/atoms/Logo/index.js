import React from 'react';
import logoWebBranca from '../../../assets/images/logo-azapfy-web-branco.webp';
import logoWeb from '../../../assets/images/logo-azapfy-web.webp';
import logoBranca from '../../../assets/images/logo-branco.webp';

import { ImgContent } from './style';
export const LogoWebBranca = ({...custom}) => <ImgContent src={logoWebBranca} {...custom}/>;
export const LogoWeb = ({...custom}) => <ImgContent src={logoWeb} {...custom} />;
export const LogoBranca = ({...custom}) => <ImgContent src={logoBranca} {...custom} />;
