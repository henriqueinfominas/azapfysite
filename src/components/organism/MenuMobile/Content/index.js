import React from 'react';
import { ContentStyled, ContentConteudoStyled } from './style';
import { useStateValue } from '../../../../state';
import { setBarraLateral } from '../../../../Reducer';
import IconeAvatar from '../../../atoms/IconeAvatar';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import ClearIcon from '@material-ui/icons/Clear';
import { primary } from '../../../../containers/themes/defaultTheme';
import Icon from '@material-ui/core/Icon';
import TypographyStyled from '../../../atoms/Typography';
import MenuMobileConteudo from '../../../moleculus/MenuMobile/Conteudo';
import { LogoWebBranca, LogoWeb } from '../../../atoms/Logo';
const ContentBarraLateral = ({ open, onClose, children }) => {
	const [ { barraLateral }, dispatch ] = useStateValue();
	const handleFechar = () => {
		onClose && onClose();
	};
	return (
		<ContentStyled className={`animated ${open ? 'slideInDown' : 'slideOutDown'} faster `}>
			<div className="container">
				<div className="d-flex mt-2">
					<LogoWeb height={40} />
					<div className="d-flex justify-content-end w-100">
						<Tooltip title="Fechar">
							<IconButton onClick={handleFechar} size="small" color={primary}>
								<ClearIcon color="primary" />
							</IconButton>
						</Tooltip>
					</div>
				</div>

				<ContentConteudoStyled className="d-flex jusitfy-content-center align-items-center mt-2">
					<MenuMobileConteudo docked={true} onClick={handleFechar}/>
				</ContentConteudoStyled>
			</div>
		</ContentStyled>
	);
};
export default ContentBarraLateral;
