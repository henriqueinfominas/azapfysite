import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import { useStateValue } from '../../../state';
import { setBarraLateral } from '../../../Reducer';
import ContentBarraLateral from './Content';
import { AppBarStyled, DialogStyled, ToolbarStyled, DialogContentStyled, ContentImagem } from './style';
const styles = {
	flex: {
		flex: 1
	}
};

const Transition = (props) => {
	return <Slide direction="left" {...props} />;
};

const BarraLateral = ({ onClose, open, classes, children }) => {
	const [ { barraLateral }, dispatch ] = useStateValue();
	const handleClose = () => {};
	return (
		<DialogStyled
			fullScreen={true}
			open={barraLateral.open}
			onClose={handleClose}
			//TransitionComponent={Transition}

			maxWidth="lg"
			PaperProps={{
				style: {
					backgroundColor: 'transparent',
					boxShadow: 'none',
					overflowY: 'hidden'
				}
			}}
		>
			<AppBarStyled elevation={0} position="fixed" />
			<DialogContentStyled className="p-0">
				<ContentBarraLateral>{children}</ContentBarraLateral>
			</DialogContentStyled>
		</DialogStyled>
	);
};

BarraLateral.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(BarraLateral);
