import React from "react";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { AvatarStyled } from "./style";
import Color from "color";
import { primary } from "../../../containers/themes/defaultTheme";
const IconeAvatar = ({
  icon,
  cor,
  iconCor,
  hover,
  iconSize,
  sizes,
  transparencia,
  img,
  radius,
  avatarSize
}) => {
  return (
    <AvatarStyled
      backgroundColor={cor ? cor : "#ccc"}
      hover={hover}
      sizes={sizes}
      transparencia={transparencia}
      radius={radius}
      avatarSize={avatarSize}
    >
      {img ? (
        img
      ) : (
        <FontAwesomeIcon
          icon={icon ? icon : faFile}
          size={iconSize ? iconSize : "xs"}
          color={iconCor ? iconCor : primary}
        />
      )}
    </AvatarStyled>
  );
};
export default IconeAvatar;
