import React from 'react';
import Ilustracao from '../../../assets/images/ilustracao.webp';
import TypographyStyled from '../../atoms/Typography';
import { RowStyled } from './style';
import CardItens from './CardItens';
import ScrollAnimation from 'react-animate-on-scroll';
const SecaoCrescer = () => {
	return (
		<section>
			<RowStyled className="row">
				<div className="col-12">
					<div className="container">
						<ScrollAnimation animateOnce animateIn="fadeInDown" >
							<TypographyStyled align="center" variant="h4" className="mt-5 mb-5 w-100">
								Quem gosta de crescer, ama o Azapfy
							</TypographyStyled>
						</ScrollAnimation>
					</div>
				</div>
				<div className="col-12 col-lg-6">
					<ScrollAnimation animateOnce animateIn="slideInLeft">
						<img className="img-fluid" src={Ilustracao} alt="Tela login app" />
					</ScrollAnimation>
				</div>
				<div className="col-12 col-lg-6">
					<CardItens />
				</div>
			</RowStyled>
		</section>
	);
};

export default SecaoCrescer;
