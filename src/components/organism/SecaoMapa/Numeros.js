import React from "react";
import { SvgLoader, SvgProxy } from "react-svgmt";
import TypographyStyled from "../../atoms/Typography";
import { ContentNossoNumeros, ContentIconeLateral } from "./style";
import IconeLateralRight from "../../../assets/icons/icone-lateral-right.svg";
import IconeLateralLeft from "../../../assets/icons/icone-lateral-left.svg";
import IconeValor from "../../atoms/IconeValor";
import {
  faMapMarkerAlt,
  faCheckCircle,
  faFileContract
} from "@fortawesome/free-solid-svg-icons";

import { primary } from "../../../containers/themes/defaultTheme";
import ScrollAnimation from 'react-animate-on-scroll';
const Numeros = () => {
  return (
    <ContentNossoNumeros>
     
      <div className="d-flex p-3">
      <ScrollAnimation animateOnce animateIn="slideInRight">
        <ContentIconeLateral>
          <SvgLoader path={IconeLateralLeft} width="40">
            <SvgProxy selector="#iconId" fill="#F15901" />
          </SvgLoader>
        </ContentIconeLateral>
        </ScrollAnimation>
        <ScrollAnimation animateOnce animateIn="fadeInDown" className="d-flex justify-content-center w-100">
        <TypographyStyled
          variant="h6"
          align="center"
          className="mt-2 w-100"
          color="#F15901"
        >
          Nossos números
        </TypographyStyled>
        </ScrollAnimation>
      </div>
      
     
      <div className="d-flex justify-content-center">
      <ScrollAnimation animateOnce animateIn="fadeInUp">
        <IconeValor
          icon={faMapMarkerAlt}
          cor="transparent"
          valorCor={primary}
          iconCor={primary}
          iconSize="2x"
          transparencia={false}
          valor={345090}
          texto={
            <span>
              COMPROVAÇÕES DE <br />
              ENTREGA POR MÊS
            </span>
          }
          coluna
          variantTexto="caption"
        />
        </ScrollAnimation>
        <ScrollAnimation animateOnce animateIn="fadeInUp">
        <IconeValor
          icon={faFileContract}
          cor="transparent"
          valorCor={primary}
          iconCor={primary}
          iconSize="2x"
          transparencia={false}
          valor={1542}
          texto={<span>CIDADES ATENDIDAS</span>}
          coluna
          variantTexto="caption"
        />
        </ScrollAnimation>
         <ScrollAnimation animateOnce animateIn="fadeInUp">
        <IconeValor
          icon={faCheckCircle}
          cor="transparent"
          valorCor={primary}
          iconCor={primary}
          iconSize="2x"
          transparencia={false}
          valor="100%"
          texto={
            <span>
              RENOVAÇÕES <br />
              CONTRATUAIS
            </span>
          }
          coluna
          variantTexto="caption"
        />
        </ScrollAnimation>
      </div>
      <div className="d-flex justify-content-end">
      <ScrollAnimation animateOnce animateIn="slideInRight" delay="2">
        <SvgLoader path={IconeLateralRight} width="40">
          <SvgProxy selector="#iconId" fill="#F15901" />
        </SvgLoader>
        </ScrollAnimation>
      </div>
    </ContentNossoNumeros>
  );
};

export default Numeros;
