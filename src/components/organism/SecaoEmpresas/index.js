import React from 'react';
import ImgDng from '../../../assets/images/empresas/dng.webp';
import ImgAmb from '../../../assets/images/empresas/amb.webp';
import ImgAcripel from '../../../assets/images/empresas/acripel.webp';
import ImgRedeMineira from '../../../assets/images/empresas/rede-mineira.webp';

import { RowStyled, ImagemEmpresa } from './style';
import ButtonConsultor from '../../moleculus/ButtonConsultor';
const SecaoEmpresas = () => {
	return (
		<section>
			<RowStyled>
				<div className="container">
					<div className="d-flex flex-wrap">
						<div className="d-flex flex-wrap justify-content-center w-100">
							<ImagemEmpresa src={ImgDng} />
							<ImagemEmpresa src={ImgAmb} />
							<ImagemEmpresa src={ImgAcripel} />
							<ImagemEmpresa src={ImgRedeMineira} />
						</div>
					</div>
					<div className="d-flex justify-content-center w-100 mt-5">
						<ButtonConsultor />
					</div>
				</div>
			</RowStyled>
		</section>
	);
};

export default SecaoEmpresas;
