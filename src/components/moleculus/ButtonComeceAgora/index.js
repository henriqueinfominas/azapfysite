import React from 'react';
import { useStateValue } from '../../../state';
import { setBarraLateral } from '../../../Reducer';
import Button from '../../atoms/Button';
import ScrollAnimation from 'react-animate-on-scroll';
const ButtonComeceAgora = () => {
	const [ {}, dispatch ] = useStateValue();
	return (
		<ScrollAnimation animateOnce animateIn="fadeInUp">
			<Button variant="contained" color="primary" onClick={() => dispatch(setBarraLateral({ open: true }))}>
				COMECE AGORA
			</Button>
		</ScrollAnimation>
	);
};
export default ButtonComeceAgora;
