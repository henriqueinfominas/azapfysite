import React from 'react';
import { ButtonStyled } from './style';
const ButtonContato = ({ children, ...props }) => {
	return <ButtonStyled {...props}>{children}</ButtonStyled>;
};

export default ButtonContato;
