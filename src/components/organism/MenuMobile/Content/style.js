import styled, { css } from 'styled-components';
export const ContentStyled = styled.div`
	height: 100%;
	width:100%;
	border: 2px solid #fff;
	background: #fff;
	z-index: 99999;
	position: fixed;
	box-shadow: -27px 20px 21px rgba(0, 0, 0, 0.16);
	overflow-y: auto;
`;
export const ContentConteudoStyled = styled.div`
	min-height: 80vh;
`;
