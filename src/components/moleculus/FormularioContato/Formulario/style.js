import styled, { css } from "styled-components";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import TextField from '@material-ui/core/TextField';
export const InputContent = styled.div`
  width: 100%;
`;
export const TextFieldStyled = styled(TextField)`
  && {
    width: 100%;
  }
`;
export const InputLabelStyled = styled(InputLabel)`
  && {
    width: 100%;
  }
`;
export const SelectStyled = styled(Select)`
  && {
    width: 100%;
  }
`;
