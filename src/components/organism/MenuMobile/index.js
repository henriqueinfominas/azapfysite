import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import { useStateValue } from '../../../state';
import { setBarraLateral } from '../../../Reducer';
import ContentMenu from './Content';
import { AppBarStyled, DialogStyled, ToolbarStyled, DialogContentStyled, ContentImagem } from './style';
const styles = {
	flex: {
		flex: 1
	}
};

const Transition = (props) => {
	return <Slide direction="left" {...props} />;
};

const ModalMobile = ({ onClose, open, classes, children }) => {
	const [ modalOpen, setModalOpen ] = useState(open);
	const handleClose = () => {
		onClose && onClose();
	};
	return (
		<DialogStyled
			fullScreen={true}
			open={open}
			onClose={handleClose}
			//TransitionComponent={Transition}

			maxWidth="lg"
			PaperProps={{
				style: {
					backgroundColor: 'transparent',
					boxShadow: 'none',
					overflowY: 'hidden'
				}
			}}
		>
			<AppBarStyled elevation={0} position="fixed" />
			<DialogContentStyled className="p-0">
				<ContentMenu open={true} onClose={handleClose}>{children}</ContentMenu>
			</DialogContentStyled>
		</DialogStyled>
	);
};

ModalMobile.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ModalMobile);
