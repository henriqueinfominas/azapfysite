import React from 'react';

import IconeValor from '../../atoms/IconeValor';
import IconDiagnostico from '../../../assets/icons/diagnostico.svg';
import IconAcompanhamento from '../../../assets/icons/acompanhamento.svg';
import IconImplantacao from '../../../assets/icons/implantacao.svg';
import { CardItensContent } from './style';
import { secondary } from '../../../containers/themes/defaultTheme';
import { SvgLoader, SvgProxy } from 'react-svgmt';
import ScrollAnimation from 'react-animate-on-scroll';
const CardItens = () => {
	return (
		<CardItensContent className="d-flex justify-content-lg-between justify-content-center flex-wrap w-100">
			<div className="m-2 mt-5">
				<ScrollAnimation animateOnce animateIn="slideInUp">
					<IconeValor
						img={<SvgLoader path={IconImplantacao} width="60px" />}
						avatarSize="auto"
						cor="transparent"
						valorCor={secondary}
						transparencia={false}
						valor="Implantação"
						texto={
							<span>
								O software é disponibilizado para a empresa<br/> cliente, que também recebe um treinamento <br/>com especialistas do Azapfy para que os<br/> funcionários entendam o funcionamento e <br/>utilizem corretamente.

							</span>
						}
						variantValor="h6"
						coluna
						className="animated fadeInUp"
					/>
				</ScrollAnimation>
			</div>

			<div className="m-2 mt-5">
				<ScrollAnimation animateOnce animateIn="slideInUp">
					<IconeValor
						img={<SvgLoader path={IconDiagnostico} width="60px" />}
						cor="transparent"
						avatarSize="auto"
						valorCor={secondary}
						transparencia={false}
						valor="Uso"
						variantValor="h6"
						className="animated fadeInUp"
						texto={
							<span>
								Com o reconhecimento e a validação do número da<br/> nota, os motoristas registram as informações da <br/>coleta à entrega em tempo real. Assim, parceiros e <br/>clientes aumentam a confiança no seu trabalho e você <br/>ganha mais credibilidade e contratos fidelizados.

							</span>
						}
						coluna
					/>
				</ScrollAnimation>
			</div>

			<div className="m-2 mt-5">
				<ScrollAnimation animateOnce animateIn="slideInUp">
					<IconeValor
						img={<SvgLoader path={IconAcompanhamento} width="60px" />}
						cor="transparent"
						avatarSize="auto"
						valorCor={secondary}
						transparencia={false}
						valor="Resultado"
						variantValor="h6"
						texto={
							<span>
								Com a visão geral das suas informações que <br/>vão da coleta à entrega, entregamos uma <br/>poderosa ferramenta de tomada de decisão, <br/>aumentando o seu nível de serviço por<br/> motorista e por região. Agora você pode <br/>crescer ainda mais!
							</span>
						}
						className="animated fadeInUp"
						coluna
					/>
				</ScrollAnimation>
			</div>
		</CardItensContent>
	);
};

export default CardItens;
