import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faTimes } from "@fortawesome/free-solid-svg-icons";
import IconButton from "@material-ui/core/IconButton";

const MenuMobile = ({ docked, mostrarMenu, onClick }) => {
  const handleClick = () => {
    onClick();
  };
  return (
    <IconButton onClick={handleClick}>
      <FontAwesomeIcon
        icon={mostrarMenu ? faTimes : faBars}
        color={docked ? "#707070" : "#fff"}
      />
    </IconButton>
  );
};
export default MenuMobile;
