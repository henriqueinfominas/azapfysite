import styled, { css } from 'styled-components';

import { ParallaxBanner } from 'react-scroll-parallax';
export const RowFooter = styled.div`height: auto !important;`;
export const FooterStyled = styled.footer`
	background-color: #ccc;
	height: auto;
`;
export const ContentFooter = styled.footer`height: 100%;`;
