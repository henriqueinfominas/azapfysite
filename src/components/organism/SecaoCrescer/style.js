import styled, { css } from 'styled-components';

import Row from '../../atoms/Row';
export const RowStyled = styled(Row)`
    padding-top: 50px;
    max-width: 100%;
`;

export const CardItensContent = styled.div``;
