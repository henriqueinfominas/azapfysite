import styled, { css } from "styled-components";
import Typography from "@material-ui/core/Typography";
export const TypographyStyled = styled(Typography)`
  && {
    color: ${props => (props.color ? props.color : "#3B3B3B")};
  }
`;
