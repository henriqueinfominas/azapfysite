import React from 'react';
import { useStateValue } from '../../../state';
import { setBarraLateral } from '../../../Reducer';
import Button from '../../atoms/Button';
import ScrollAnimation from 'react-animate-on-scroll';
const ButtonConsultor = () => {
	const [ {  }, dispatch ] = useStateValue();
	return (
		<ScrollAnimation animateOnce animateIn="fadeInUp">
			<Button variant="contained" color="secondary" onClick={() => dispatch(setBarraLateral({ open: true }))}>
				FALE COM UM CONSULTOR
			</Button>
		</ScrollAnimation>
	);
};
export default ButtonConsultor;
