import React from 'react';
import { CardItensContent } from './style';
import TypographyStyled from '../../atoms/Typography';
import { primary } from '../../../containers/themes/defaultTheme';
import ScrollAnimation from 'react-animate-on-scroll';
const CardItens = () => {
	return (
		<CardItensContent className="d-flex flex-column flex-wrap p-2">
			<ScrollAnimation animateOnce animateIn="fadeIn">
				<div className="mt-2">
					<TypographyStyled variant="subheading" align="start" className="mt-2" color={primary}>
						PARCEIROS:
					</TypographyStyled>
					<TypographyStyled variant="p" align="start" className="mt-2">
						Transportadores entregam rápido, fornecem informações em real<br />
						time e mantem seus contratos ativos, com clientes satisfeitos.
					</TypographyStyled>
				</div>
			</ScrollAnimation>
			<ScrollAnimation animateOnce animateIn="fadeIn">
				<div className="mt-2">
					<TypographyStyled variant="subheading" align="start" className="mt-2" color={primary}>
						GERENTES GERAIS:
					</TypographyStyled>
					<TypographyStyled variant="p" align="start" className="mt-2">
						Têm mais controle sobre a sua operação de gestão
						<br />
						e comprovação da entrega e destaque frente <br /> a diretoria.
					</TypographyStyled>
				</div>
			</ScrollAnimation>
			<ScrollAnimation animateOnce animateIn="fadeIn">
				<div className="mt-2">
					<TypographyStyled variant="subheading" align="start" className="mt-2" color={primary}>
						AGREGADOS:
					</TypographyStyled>
					<TypographyStyled variant="p" align="start" className="mt-2">
						Nunca mais pagam por mercadorias que ele entregou e não comprovou, <br />
						já que 100% será comprovado.
					</TypographyStyled>
				</div>
			</ScrollAnimation>
			<ScrollAnimation animateOnce animateIn="fadeIn">
				<div className="mt-2">
					<TypographyStyled variant="subheading" align="start" className="mt-2" color={primary}>
						DIRETORES:
					</TypographyStyled>
					<TypographyStyled variant="p" align="start" className="mt-2">
						Se sentem prontos para conquistar novos clientes e <br />
						crescer apresentando um diferencial significativo.
					</TypographyStyled>
				</div>
			</ScrollAnimation>
		</CardItensContent>
	);
};

export default CardItens;
