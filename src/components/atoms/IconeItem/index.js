import React from "react";
import { ContentStyled, ContentTypography } from "./style";
import IconeAvatar from "../IconeAvatar";
const IconeValor = ({
  icon,
  cor,
  valor,
  tipo,
  texto,
  onClick,
  valorCor,
  textCor,
  iconCor,
  transparencia,
  img,
  variantTexto,
  variantValor,
  radius
}) => {
  const handleClick = () => {
    onClick && onClick({ tipo, valor, texto });
  };
  return (
    <ContentStyled onClick={handleClick} action={onClick}>
      <div className="d-flex">
        <IconeAvatar
          icon={icon}
          cor={cor ? cor : "#ccc"}
          hover={onClick}
          transparencia={transparencia}
          iconCor={iconCor}
          img={img}
          radius={radius}
        />
        <div className="d-flex flex-column justify-content-center ml-2 ">
          {valor && (
            <ContentTypography
              variant={variantValor ? variantValor : "body2"}
              color={valorCor ? valorCor : textCor}
            >
              {valor}
            </ContentTypography>
          )}
          {texto && (
            <ContentTypography
              variant={variantTexto ? variantTexto : "caption"}
              color={textCor}
            >
              {texto}
            </ContentTypography>
          )}
        </div>
      </div>
    </ContentStyled>
  );
};
export default IconeValor;
