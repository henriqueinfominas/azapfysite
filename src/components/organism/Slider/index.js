import React from 'react';
import BackgroundLaranja from '../../../assets/images/slider/background-laranja-azapfy.webp';
import TelaDashboardSmartphone from '../../../assets/images/slider/tela-dash-smartphone.webp';
import { SliderContent, RowSlider } from './style';
import ScrollAnimation from 'react-animate-on-scroll';
import Carousel from 'react-bootstrap/Carousel';
import TypographyStyled from '../../atoms/Typography';
import Button from '../../atoms/Button';
import { NavLink } from 'react-router-dom';
import { useStateValue } from '../../../state';
import { setBarraLateral } from '../../../Reducer';
const Slider = () => {
	const [ { barraLateral }, dispatch ] = useStateValue();
	return (
		<Carousel>
			<Carousel.Item>
				<RowSlider backgroundImage={BackgroundLaranja} className="d-flex align-items-center pt-5 h-100">
					<SliderContent className="container">
						<div className="row">
							<div className="col-12 col-lg-6">
								<ScrollAnimation animateOnce animateIn="fadeInDown">
									<TypographyStyled variant="h5" color="#ffffff" className="mt-5 mt-lg-3">
										Seja líder em produtividade e aumente o seu nível de serviço com<br />
										o mais completo sistema de comprovação de coletas e entregas!
									</TypographyStyled>
								</ScrollAnimation>
								<ScrollAnimation animateOnce animateIn="fadeInDown">
									<TypographyStyled variant="p" color="#ffffff" className="mt-5 mt-lg-3">
										O Azapfy é uma solução ágil, inovadora e confiável<br /> que chegou para
										transformar a produtividade das<br /> empresas na gestão de últimas milhas.
									</TypographyStyled>
								</ScrollAnimation>
								<div className="d-flex justify-content-center w-100 mt-5 ">
									<ScrollAnimation animateOnce animateIn="fadeInUp" delay="2">
										<div
											className="d-none d-sm-none d-md-none d-lg-inline mt-2"
											onClick={() => dispatch(setBarraLateral({ open: true }))}
										>
											<Button variant="contained" color="secondary">
												EXPERIMENTAR
											</Button>
										</div>
									</ScrollAnimation>
								</div>
							</div>
							<div className="col-12 col-lg-6 ">
								<img
									className="img-fluid mb-2 animated fadeInRight"
									src={TelaDashboardSmartphone}
									alt="Tela Dashboard e smartphone"
								/>
							</div>
							<div className="d-flex justify-content-center w-100 mt-2 mb-3">
								<ScrollAnimation animateOnce animateIn="fadeInUp" delay="2">
									<Button
										variant="contained"
										color="secondary"
										className=" d-block d-lg-none"
										onClick={() => dispatch(setBarraLateral({ open: true }))}
									>
										EXPERIMENTAR
									</Button>
								</ScrollAnimation>
							</div>
						</div>
					</SliderContent>
				</RowSlider>
			</Carousel.Item>
		</Carousel>
	);
};
export default Slider;
