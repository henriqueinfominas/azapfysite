import React from 'react';
import AppTelaLogin from '../../../assets/images/app-tela-login-circulo.webp';
import TypographyStyled from '../../atoms/Typography';
import { RowStyled } from './style';
import CardItens from './CardItens';
import ScrollAnimation from 'react-animate-on-scroll';
import ButtonConsultor from '../../moleculus/ButtonConsultor';
const Sobre = () => {
	return (
		<section>
			<RowStyled>
				<div className="container">
					<div className="row">
						<div className="col-12 col-lg-6 ">
							<div className="d-flex justify-content-center w-100">
								<ScrollAnimation animateOnce animateIn="fadeInLeft" className=" w-75" delay="1">
									<img className="img-fluid " src={AppTelaLogin} alt="Tela login app azapfy" />
								</ScrollAnimation>
							</div>
						</div>
						<div className="col-12 col-lg-6 ">
							<ScrollAnimation animateOnce animateIn="fadeInDown">
								<TypographyStyled variant="h5" className="mt-5">
									O que o Azapfy faz pela sua transportadora?
								</TypographyStyled>
							</ScrollAnimation>
							<ScrollAnimation animateOnce animateIn="fadeInDown">
								<TypographyStyled variant="p" component="p">
									Informação em tempo real que garante a satisfação do seu cliente e mais segurança
									para a sua operação.
								</TypographyStyled>
							</ScrollAnimation>
							<CardItens />
						</div>
						<div className="d-flex justify-content-center w-100 mt-5">
							<ButtonConsultor />
						</div>
					</div>
				</div>
			</RowStyled>
		</section>
	);
};

export default Sobre;
