import styled, { css } from "styled-components";
import Avatar from "@material-ui/core/Avatar";
import Color from "color";
export const AvatarStyled = styled(Avatar)`
  && {
    
    ${props =>
      !props.radius &&
      css`
        border-radius: 0px;
      `}
    ${props =>
      props.avatarSize &&
      css`
        width:${props => props.avatarSize}
      height:${props => props.avatarSize}
      `}
    background-color: ${props =>
      props.transparencia
        ? Color(props.backgroundColor)
            .fade(0.9)
            .string()
        : props.backgroundColor};
    ${props =>
      props.hover &&
      css`
        :hover {
          background-color: ${props =>
            Color(props.backgroundColor)
              .fade(0.7)
              .string()};
          cursor: pointer;
        }
      `}
  }
`;
