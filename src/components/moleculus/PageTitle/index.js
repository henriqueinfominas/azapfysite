import React from "react";
import TypographyStyled from "../../atoms/Typography";
import { Content } from "./style";
const PageTitle = ({ titulo }) => {
  return (
    <Content>
      <div className="container">
      <TypographyStyled component="h1" variant="h4">{titulo}</TypographyStyled>
      </div>
    </Content>
  );
};

export default PageTitle;
