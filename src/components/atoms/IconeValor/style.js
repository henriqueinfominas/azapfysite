import styled, { css } from "styled-components";

import Typography from "@material-ui/core/Typography";

export const ContentStyled = styled.div`
  display: flex;
  justify-self: center;
  cursor: ${props => (props.action ? "pointer" : "default")};
`;
export const ContentTypography = styled(Typography)`
  && {
    ${props =>
      props.color &&
      css`
        color: ${props => props.color};
      `}
  }
`;
