import { create } from 'apisauce';
const api = create({
	baseURL: 'http://homologacao.azapfy.com.br',
	headers: { Accept: 'application/json' }
});

export default api;
