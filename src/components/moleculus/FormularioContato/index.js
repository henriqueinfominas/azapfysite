import React, { useState } from 'react';
import ReactDOMServer from 'react-dom/server';
import api from '../../../api';
import ModeloBody from './ModeloBody';
import MensagemConfirmacao from './MensagemConfirmacao';
import Formulario from './Formulario';
import TypographyStyled from '../../atoms/Typography';
const FormularioContato = () => {
	const [ enviado, setEnviado ] = useState(false);
	const enviarEmailReq = async (dados) =>
		api.post('/api/notas/email', dados).then((dados) => dados).catch((error) => error);
	const enviarEmail = ({ values, actions }) => {
		let dados = {
			nome_remetente: values.nome,
			remetente: values.email,
			destinatario: [ 'h.freire02@gmail.com', 'marketing@azapfy.com.br', 'azapfyvpf@gmail.com' ], //'marketing@azapfy.com.br', 'azapfyvpf@gmail.com'
			assunto: `Mensagem Site [ ${values.areaAtuacao} ]`,
			tipo: 'site',
			body: ReactDOMServer.renderToString(
				<ModeloBody
					nome={values.nome}
					empresa={values.empresa}
					cargo={values.cargo}
					areaAtuacao={values.areaAtuacao}
					telefone={values.telefone}
				/>
			)
		};
		let mensagem = '';
		try {
			const response = enviarEmailReq(dados);
			response.then((response) => {
				if (response.ok) {
					actions.setSubmitting(false);
					if (response.data.resultado == false) {
						console.log('response resuyltado ', response);
						actions.setErrors({ enviar: 'Erro ao enviar o e-mail tente novamente.' });
						return false;
					}

					setEnviado(true);
					actions.resetForm();
				} else {
					console.log('response ', response);
					actions.setErrors({ enviar: 'Erro ao enviar o e-mail tente novamente.' });
				}
			});
		} catch (error) {
			console.log('error catch', error);
			//setloading(false);
			actions.setErrors({ enviar: 'Erro ao enviar o e-mail tente novamente.' });
			actions.setSubmitting(false);
		}
	};
	return (
		<React.Fragment>
			{enviado ? (
				<MensagemConfirmacao />
			) : (
				<div>
					<TypographyStyled variant="h4">Preencha o formulário que entraremos em contato.</TypographyStyled>
					<Formulario onEnviarEmail={enviarEmail} />
				</div>
			)}
		</React.Fragment>
	);
};

export default FormularioContato;
