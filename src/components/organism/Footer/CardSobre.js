import React from "react";
import TypographyStyled from "../../atoms/Typography";
const CardSobre = () => {
  return (
    <TypographyStyled variant="subheading" color="#fff">
      O AZAPFY foi criado para transformar a produtividade da sua
      transportadora, somos apaixonados por tecnologia e pelo poder que ela tem
      de transformar positivamente a forma de você lucrar mais e crescer,
      fazendo + com - 
    </TypographyStyled>
  );
};

export default CardSobre;
