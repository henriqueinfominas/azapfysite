import styled from 'styled-components';
import AppBar from '@material-ui/core/AppBar';
import Dialog from '@material-ui/core/Dialog';
import Toolbar from '@material-ui/core/Toolbar';
import DialogContent from '@material-ui/core/DialogContent';
export const AppBarStyled = styled(AppBar)`
&&{
    background-color: transparent;
 
}`;
export const DialogStyled = styled(Dialog)`
&&{
    background-color: rgba(0,0,0,0.5);
}`;
export const ToolbarStyled = styled(Toolbar)`
&&{
    background-color: transparent;
}`;
export const DialogContentStyled = styled(DialogContent)`
&&{
    background-color: transparent;
    height: 100%;
    width:100%;
    overflow: auto;
}`;

