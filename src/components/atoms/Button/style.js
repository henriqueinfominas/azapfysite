import styled, { css } from 'styled-components';
import Button from '@material-ui/core/Button';

export const ButtonStyled = styled(Button)`
&&{
    border-radius: 50px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 50px;
    padding-right: 50px;
    height: 100%;
    
}
`;
