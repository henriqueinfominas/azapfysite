import React from 'react';
import Row from '../../atoms/Row';
import FormularioContato from '../../moleculus/FormularioContato';

const ConteudoContato = () => {
	return (
		<section>
			<Row >
				<div className="container d-flex align-itens-center">
					<FormularioContato />
				</div>
			</Row>
		</section>
	);
};

export default ConteudoContato;
