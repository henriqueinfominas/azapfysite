import styled, { css } from "styled-components";

export const RowStyled = styled.div`
  ${props =>
    props.backgroundImage &&
    !props.overlay &&
    css`
      background-image: url(${props => props.backgroundImage});
    `};
  ${props =>
    props.backgroundColor &&
    !props.overlay &&
    css`
      background-color: ${props => props.backgroundColor};
    `};
  ${props =>
    props.overlay &&
    css`
      background: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)),
        url(${props => props.backgroundImage});
    `};
	${props =>
    props.default &&
    css`
     min-height: 100vh;
    `};
  
  background-repeat: no-repeat, repeat;
  background-size: cover;
  background-position: center;

`;
