import React from 'react';

//Styled components
import TypographyStyled from '../../atoms/Typography';
import Button from '../../atoms/Button';
import { NavLink } from 'react-router-dom';
import { RowStyled, ContentCall } from './style';
import ScrollAnimation from 'react-animate-on-scroll';
import ButtonComeceAgora from '../../moleculus/ButtonComeceAgora';
const CallToAction = () => {
	return (
		<section>
			<RowStyled>
				<ContentCall className="container d-flex justify-content-lg-between justify-content-md-between  justify-content-sm-between justify-content-center flex-wrap">
					<div>
						<ScrollAnimation animateOnce animateIn="fadeInDown">
							<TypographyStyled variant="h6" color="#fff">
								Faça como várias transportadoras no Brasil
							</TypographyStyled>
						</ScrollAnimation>
						<ScrollAnimation animateOnce animateIn="fadeInDown">
							<TypographyStyled component="p" color="#fff">
								Transforme a gestão das suas entregas com o Azapfy
							</TypographyStyled>
						</ScrollAnimation>
					</div>
					<ButtonComeceAgora />
				</ContentCall>
			</RowStyled>
		</section>
	);
};

export default CallToAction;
