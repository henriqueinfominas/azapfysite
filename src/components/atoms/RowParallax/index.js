import React from "react";
import { RowStyled } from "./style";
const RowParallax = ({ children, ...props }) => {
  return <RowStyled {...props}>{children}</RowStyled>;
};

export default RowParallax;
