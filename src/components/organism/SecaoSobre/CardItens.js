import React from 'react';

import IconeValor from '../../atoms/IconeValor';
import AumentoinstataneoIcon from '../../../assets/icons/aumentoinstataneo.webp';
import ConecteIcon from '../../../assets/icons/conecte.webp';
import NaodeixedereceberIcon from '../../../assets/icons/naodeixedereceber.webp';
import RotasIcon from '../../../assets/icons/rotas.webp';
import { CardItensContent } from './style';
import ScrollAnimation from 'react-animate-on-scroll';
const CardItens = () => {
	return (
		<CardItensContent className="d-flex flex-column flex-wrap">
			<div className="d-flex flex-wrap">
				<div className="m-2 mt-5">
				<ScrollAnimation  animateIn="fadeIn" delay="1">
					<IconeValor
						img={<img src={AumentoinstataneoIcon} width="40px" />}
						cor="transparent"
						iconCor="#FF8000"
						transparencia={false}
						valor="Aumento instantâneo"
						texto="de competitividade"
						variantValor="caption"
					/>
					</ScrollAnimation>
				</div>
				<div className="m-2 mt-5 animated fadeIn fadeInDown">
				<ScrollAnimation  animateIn="fadeIn" delay="1">
					<IconeValor
						img={<img src={ConecteIcon} width="40px" />}
						cor="transparent"
						iconCor="#FF8000"
						transparencia={false}
						valor="Conecte o AZAPFY com"
						texto="todos os seus parceiros"
						variantValor="caption"
					/>
					</ScrollAnimation>
				</div>
			</div>
			<div className="d-flex flex-wrap">
				<div className="m-2 mt-5 animated fadeIn fadeInDown">
				<ScrollAnimation  animateIn="fadeIn" delay="1">
					<IconeValor
						img={<img src={RotasIcon} width="40px" />}
						cor="transparent"
						iconCor="#FF8000"
						transparencia={false}
						valor="Tenha rotas inteligentes"
						texto="geradas automaticamente "
						variantValor="caption"
					/>
					</ScrollAnimation>
				</div>
				<div className="m-2 mt-5 animated fadeIn fadeInDown">
				<ScrollAnimation  animateIn="fadeIn" delay="1">
					<IconeValor
						img={<img src={NaodeixedereceberIcon} width="40px" />}
						cor="transparent"
						iconCor="#FF8000"
						transparencia={false}
						valor="Nunca mais deixe de receber "
						texto="por falta de comprovação!"
						variantValor="caption"
					/>
					</ScrollAnimation>
				</div>
			</div>
		</CardItensContent>
	);
};

export default CardItens;
