import React from 'react';
import { SliderContent, RowSlider } from '../Slider/style';
import { SliderImagemContent } from './style';
import Carousel from 'react-bootstrap/Carousel';
import TypographyStyled from '../../atoms/Typography';
import BackgroundSistema from '../../../assets/images/slider/fundo-slide-sistema.webp';
import TelaColeta from '../../../assets/images/telas/coleta.webp';
import TelaAcompanhamento from '../../../assets/images/telas/acompanhamento.webp';
import TelaEntrega from '../../../assets/images/telas/entrega.webp';
import TelaViagem from '../../../assets/images/telas/viagem.webp';
import { primary } from '../../../containers/themes/defaultTheme';
import IconeAvatar from '../../atoms/IconeAvatar';
import { faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
const Slider = () => {
	return (
		<Carousel
			nextIcon={
				<IconeAvatar icon={faChevronRight} transparencia={false} iconCor="#707070" cor="transparent" hover />
			}
			prevIcon={
				<IconeAvatar icon={faChevronLeft} transparencia={false} iconCor="#707070" cor="transparent" hover />
			}
		>
			<Carousel.Item>
				<RowSlider backgroundImage={BackgroundSistema} className="d-flex align-items-center pt-5 h-100">
					<SliderContent className="container">
						<div className="row">
							<div className="col-12">
								<TypographyStyled variant="h5" align="center" className="mb-3 w-100" color={primary}>
									Como Funciona?
								</TypographyStyled>
								<TypographyStyled variant="h5" align="center" className="w-100" color={primary}>
									1-COLETA
								</TypographyStyled>

								<TypographyStyled variant="subheading" color="#707070" align="center">
									Bipagem das notas da distribuidora e disponibilização da informação no sistema
								</TypographyStyled>

								<img src={TelaColeta} alt="Tela Coleta" className="img-fluid" />
							</div>
						</div>
					</SliderContent>
				</RowSlider>
			</Carousel.Item>
			<Carousel.Item>
				<RowSlider backgroundImage={BackgroundSistema} className="d-flex align-items-center pt-5 h-100">
					<SliderContent className="container">
						<div className="row">
							<div className="col-12">
								<TypographyStyled variant="h5" align="center" className="mb-3 w-100" color={primary}>
									Como Funciona?
								</TypographyStyled>
								<TypographyStyled variant="h5" align="center" className="w-100" color={primary}>
									2-VIAGEM
								</TypographyStyled>

								<TypographyStyled variant="subheading" color="#707070" align="center">
									Criação de Romaneios e acompanhamento das mercadorias em trânsito com informação em
									tempo real
								</TypographyStyled>

								<SliderImagemContent>
									<img src={TelaViagem} alt="Tela Viagem" className="img-fluid" />
								</SliderImagemContent>
							</div>
						</div>
					</SliderContent>
				</RowSlider>
			</Carousel.Item>
			<Carousel.Item>
				<RowSlider backgroundImage={BackgroundSistema} className="d-flex align-items-center pt-5 h-100">
					<SliderContent className="container">
						<div className="row">
							<div className="col-12">
								<TypographyStyled variant="h5" align="center" className="mb-3 w-100" color={primary}>
									Como Funciona?
								</TypographyStyled>
								<TypographyStyled variant="h5" align="center" className="w-100" color={primary}>
									3-ENTREGA
								</TypographyStyled>

								<TypographyStyled variant="subheading" color="#707070" align="center">
									Comprovação das entregas realizadas pela validação automática do número dos canhotos
								</TypographyStyled>

								<SliderImagemContent>
									<img src={TelaEntrega} alt="Tela Entrega" className="img-fluid" />
								</SliderImagemContent>
							</div>
						</div>
					</SliderContent>
				</RowSlider>
			</Carousel.Item>
			<Carousel.Item>
				<RowSlider backgroundImage={BackgroundSistema} className="d-flex align-items-center pt-5 h-100">
					<SliderContent className="container">
						<div className="row">
							<div className="col-12">
								<TypographyStyled variant="h5" align="center" className="mb-3 w-100" color={primary}>
									Como Funciona?
								</TypographyStyled>
								<TypographyStyled variant="h5" align="center" className="w-100" color={primary}>
									4-ACOMPANHAMENTO
								</TypographyStyled>

								<TypographyStyled variant="subheading" color="#707070" align="center">
									Informações completas sempre disponíveis para o acompanhamento de todos os clientes
									e parceiros
								</TypographyStyled>

								<SliderImagemContent>
									<img src={TelaAcompanhamento} alt="Tela Acompanhamento" className="img-fluid" />
								</SliderImagemContent>
							</div>
						</div>
					</SliderContent>
				</RowSlider>
			</Carousel.Item>
		</Carousel>
	);
};
export default Slider;
